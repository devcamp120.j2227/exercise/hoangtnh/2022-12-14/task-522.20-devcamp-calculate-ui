import styled from "styled-components";

const HeaderForm = styled.text`
${props => props.type && props.type ==="array-number" ? 
    `color: gray; font-size: 1.8rem;`:
    `color: white; font-size:2.8rem;`
}
padding:10px;
display: flex;
justify-content:end;
`;
export default HeaderForm;