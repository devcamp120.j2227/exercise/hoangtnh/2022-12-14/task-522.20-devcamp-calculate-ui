import { Col, Container, Row } from "reactstrap";
import FormCalculate from "./components/calculateForm";
import "bootstrap/dist/css/bootstrap.min.css"
import HeaderForm from "./components/headerForm";
import "./App.css"
import ButtonMath from "./components/buttonMath";
import ButtonC from "./components/buttonC";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
function App() {
  return (
      <Container className="mt-5">
        <FormCalculate >
            <HeaderForm type="array-number">
              <Row>
                <p>2536 + 419 +</p>
              </Row>
            </HeaderForm>
            <hr/>
            <HeaderForm>
              <Row>
                <p>2955</p>
              </Row>
            </HeaderForm>
            <Col>
              <ButtonC>
                C
              </ButtonC>
              <ButtonMath>
                ≠
              </ButtonMath>
              <ButtonMath>
                %
              </ButtonMath>
              <ButtonMath>
                /
              </ButtonMath>
            </Col>
            <Col>
              <ButtonMath type="normal">
                7
              </ButtonMath>
              <ButtonMath type="normal">
                8
              </ButtonMath>
              <ButtonMath type="normal">
                9
              </ButtonMath>
              <ButtonMath>
                *
              </ButtonMath>
            </Col>
            <Col>
              <ButtonMath type="normal">
                4
              </ButtonMath>
              <ButtonMath type="normal">
                5
              </ButtonMath>
              <ButtonMath type="normal">
                6
              </ButtonMath>
              <ButtonMath>
                -
              </ButtonMath>
            </Col>
            <Col>
              <ButtonMath type="normal">
                1
              </ButtonMath>
              <ButtonMath type="normal">
                2
              </ButtonMath>
              <ButtonMath type="normal">
                3
              </ButtonMath>
              <ButtonMath>
                +
              </ButtonMath>
            </Col>
            <Col>
              <ButtonMath type="normal">
                .
              </ButtonMath>
              <ButtonMath type="normal">
                0
              </ButtonMath>
              <ButtonMath type="normal">
                <ArrowBackIosIcon fontSize="small"/>
              </ButtonMath>
              <ButtonMath>
                =
              </ButtonMath>
            </Col>
        </FormCalculate>
      </Container>
  );
}

export default App;
